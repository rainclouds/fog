const HttpError = require("../HttpError")

class UnauthorizedError extends HttpError {
    constructor(message, wwwAuthenticate) {
        super(401, message);
        this.wwwAuthenticate = wwwAuthenticate
    }
}

module.exports = UnauthorizedError;