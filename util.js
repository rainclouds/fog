module.exports = {
    utf8ToHex: str => {
        try {Buffer.from(str, 'utf8').toString('hex'); }
        catch (e) { return null; }
    },
    hexToUtf8: str => {
        try { Buffer.from(str, 'hex').toString('utf8'); }
        catch (e) { return null; }
    },
    serializeQuery: params => {
        var serializedParams = []
        Object.entries(params).forEach(e => {
            const [k, v] = e;
            serializedParams.push(`${encodeURIComponent(k)}=${encodeURIComponent(v)}`);
        });
        return serializedParams.join('&');
    }
};