const storeSession = (sessionKey, session) => {
    window.localStorage.setItem(sessionKey, session);
}

const getSession = (sessionKey) => {
    return window.localStorage.getItem(sessionKey);
}