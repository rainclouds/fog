Verify your Fog Email Address

Thank you for registering your account with Fog, Rain Cloud's official authentication service!
To get verify that the email address you associated to your Fog account is valid, please visit the following link.
${verificationUrl}