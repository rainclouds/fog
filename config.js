const util = require('util');
const fs = require('fs');

const parseBool = (value, default_value) => {
    if(value !== 'true' && value !== 'false') return default_value;
    else if (value === 'true') return true;
    else if (value === 'false') return false;
}

module.exports = {
    // SERVICE CONFIGURATION
    service_domain: process.env.SERVICE_DOMAIN || "127.0.0.1",
    service_root: process.env.SERVICE_ROOT || "/service",
    session_default_ttl: process.env.SESSION_DEFAULT_TTL || 3600,
    enrollment_rules: {
        require_email: parseBool(process.env.ENROLL_REQUIRE_EMAIL, true),
        require_email_verification: parseBool(process.env.ENROLL_REQUIRE_VERIFY_EMAIL, true),
        require_phone: parseBool(process.env.ENROLL_REQUIRE_PHONE, true),
        allowance: process.env.ENROLLMENT_ALLOWANCE || "closed"
    },
    // REDIS CONFIGURATION
    redis_conn_str: util.format("redis://%s:%s@%s",
        process.env.REDIS_USERNAME,
        process.env.REDIS_PASSWORD,
        process.env.REDIS_HOSTNAME),

    redis_conn_str_masked: util.format("redis://%s:********@%s",
        process.env.REDIS_USERNAME,
        process.env.REDIS_HOSTNAME),

    // DATABASE CONFIGURATION
    mongo_options: {
        autoIndex: true
    },
    mongo_conn_str: util.format("%s%s:%s@%s/%s%s",
        process.env.MONGO_PROTOCOL,
        process.env.MONGO_USERNAME,
        process.env.MONGO_PASSWORD,
        process.env.MONGO_HOSTNAME,
        process.env.MONGO_FOGDB_NAME,
        process.env.MONGO_CONN_ARGS),

    mongo_conn_str_masked: util.format("%s%s:********@%s/%s%s",
        process.env.MONGO_PROTOCOL,
        process.env.MONGO_USERNAME,
        process.env.MONGO_HOSTNAME,
        process.env.MONGO_FOGDB_NAME,
        process.env.MONGO_CONN_ARGS),

    // HTTP CONFIGURATION
    serve_http: parseBool(process.env.SERVE_HTTP, true),
    service_http_port: process.env.SERVICE_HTTP_PORT || 3080,
    service_http_address: process.env.SERVICE_HTTP_ADDRESS || "127.0.0.1",

    // HTTPS CONFIGURATION
    serve_https: parseBool(process.env.SERVE_HTTPS, false),
    service_https_port: process.env.SERVICE_HTTPS_PORT || 3443,
    service_https_address: process.env.SERVICE_HTTPS_ADDRESS || "127.0.0.1",
    service_certificate: {
        key: process.env.SERVICE_KEY ? fs.readFileSync(process.env.SERVICE_KEY, 'utf8') : null,
        cert: process.env.SERVICE_CERT ? fs.readFileSync(process.env.SERVICE_CERT, 'utf8') : null,
        ca: process.env.SERVICE_CA ? fs.readFileSync(process.env.SERVICE_CA, 'utf8') : null
    },

    // SMTP Configuration
    smtp_transporter: {
        host: process.env.SMTP_HOSTNAME,
        port: process.env.SMTP_PORT,
        auth: {
            type: "login",
            user: process.env.SMTP_USERNAME,
            pass: process.env.SMTP_PASSWORD
        },
        secure: !parseBool(process.env.SMTP_USE_TLS, false),
        secureConnection: !parseBool(process.env.SMTP_USE_TLS, false),
        tls: parseBool(process.env.SMTP_USE_TLS, false) ? {
            rejectUnauthorized: parseBool(process.env.SMTP_TLS_REJECT_UNAUTHORIZED, false),
            ciphers: process.env.SMTP_TLS_CIPHERS || 'SSLv3'
        } : null,

    },
    system_from_address: process.env.SYSTEM_FROM_EMAIL,
    email_templates: {
        verification: {
            subject: process.env.EMAIL_VERIFY_SUBJECT,
            // text: open(process.env.EMAIL_VERIFY_TEXT, 'r'),
            pug: process.env.EMAIL_VERIFY_TEMPLATE
        }
    }
}