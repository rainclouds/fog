# Fog Authenticator
Fog is the official authentication provider for the RainClouds server stack. It integrates Single Sign On authentication techniques, but also secures and authorizes requests for cloud-to-cloud transactions.
## Authentication
Authentication using a username and password allows a user to login to their credential. A credential on its own, acts as no more than a lock for a set of identities. A credential on its own will allow a user to login and manage identities, but it in itself does not contain any soft of personal information about the user.

### Session Management
Sessions are created and destroyed through the authenticate route. Sessions are stored in redis and are accessed with the key pattern `fog.sessions:{_credentialsID}:{sessionToken}`.
The following data makes up a session:
  - _credentialsID - ID associated to the master user account
  - sessionToken - Used to identify the session, ties server and client sessions together
  - clientSecret - Used to verify the integrity of the client during requests
  - serverSecret - Used to verify the integrity of the remote service the client is logged into during requests
  - expiry - Determines the life cycle of the session, set to 0 for no expiry, value updates as activity occurs on the given session
  - expirationTimestamp - A human-readable ISOString timestamp of expiry
  - sessionKey - The key used in redis to fetch and set the session
  - permits - Temporary permissions that allow the client to give the remote service access to resources on the end-user's host system.

### Session Permits
Permits are temporary permission claims a client session makes so that the server session can request resources from the end-user's cloud.
Permits are stored in sessions and are destroyed when the session is destroyed. For more permanent permissions, see the Authorization section.
The body of a permit is made up of the following:
  - expiry - Determines how long access is granted for
  - resourceKey - Determines the resource we are giving access to
  - permissionKey - Determines what permissions the server session has to a given resource (ro, rw, w, x)

## Identity Management
An identity is a profile associated to a set of credentials. You can have many identities to a single set of credentials. This allows a single user to manage multiple accounts and logins under a single set of credentials.