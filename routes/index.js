const AuthenticationService = require("../services/AuthenticationService");
const SessionService = require("../services/SessionService");
const HttpError = require("../HttpError");
const crypto = require("crypto");


module.exports = (app, rootPath, redisClient) => {
    const localRoot = rootPath;

    app.get(localRoot + '/', async (req, res) => {
        return res.status(200).json({status: "live", service: "fog", description: "Authentication service for the Raincloud software stack."});
    });
}