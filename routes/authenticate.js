const AuthenticationService = require("../services/AuthenticationService");
const SessionService = require("../services/SessionService");
const EmailService = require("../services/EmailService");
const HttpError = require("../HttpError");
const crypto = require("crypto");
const bodyParser = require("body-parser");
const CONFIG = require("../config");
const util = require("../util")
const Validator = require("express-json-validator-middleware").Validator;
const EnrollmentContract = require("../contracts/authentication/EnrollmentContract");


module.exports = (app, rootPath, redisClient) => {
    const localRoot = rootPath + '/authenticate';
    const svcAuthentication = new AuthenticationService();
    const svcSession = new SessionService(redisClient);
    const svcEmail = new EmailService(CONFIG.smtp_transporter);
    const {validate}  = new Validator();

    // Credential Authentication
    app.post(localRoot + '/', async (req, res, next) => {
        try {
            const b64auth = (req.headers.authorization || '').split(' ')[1] || '';
            const [signOn, password] = Buffer.from(b64auth, 'base64').toString().split(':')
            const hash = crypto.createHash("sha256").update(password).digest("hex");
            const credentials = await svcAuthentication.authenticate(signOn, hash);
            const session = await svcSession.storeSession(credentials._id);
            return res.status(200).json(session);
        } catch (err) { return next(err); }
    });


    // Credential Enrollment
    app.post(localRoot + "/enroll", bodyParser.json(), validate(EnrollmentContract), async (req, res, next) => {
        try {
            if (!req.headers.authorization) return next(new HttpError(400, "Expected an authorization header."));
            const b64auth = req.headers.authorization.split(' ')[1] || null;
            if (b64auth === null) return next(new HttpError(400, "Invalid authorization header."));
            const [signOn, password] = Buffer.from(b64auth, 'base64').toString().split(':');
            const hash = crypto.createHash("sha256").update(password).digest("hex");
            const credentials =  await svcAuthentication.enroll(signOn, hash, req.body?.email || null, req.body?.phone || null);
            const host = `${req.protocol}://${CONFIG.service_domain}${CONFIG.service_root}`;
            if (CONFIG.enrollment_rules.require_email_verification) await svcEmail.sendVerificationMail(credentials.email, credentials.emailVerificationCode, host);
            return res.status(201).json({'status': 'enrolled'});
        } catch(err) { return next(err); }
    });


    // Session Destruction
    app.post(localRoot + "/logout", async (req, res, next) => {
        try {
            if(!req.headers['fog-session-key']) return next(new HttpError(422, "Required header, fog-session-key, not provided."));
            if(!await svcSession.fetchSession(req.headers['fog-session-key'], false)) return res.status(204).send();
            console.log(`Destroying Session fog.sessions:${req.headers['fog-session-key']}...`);
            await svcSession.popSession(req.headers['fog-session-key']);
            return res.status(204).send();
        } catch (err) { return next(err); }
    });

    // Email Verification
    app.post(localRoot + "/verify", async (req, res, next) => {
        try {
            const credentials = await svcAuthentication.verifyEmail(util.hexToUtf8(req.query.i || ""), req.query.k || null);
            const session = await svcSession.storeSession(credentials._id);
            return res.status(200).json(session);
        } catch (err) { return next(err); }
    });
}