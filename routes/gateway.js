const util = require('../util');
const HttpError = require('../HttpError');
const AuthenticationService = require("../services/AuthenticationService");
const SessionService = require("../services/SessionService");
const crypto = require('crypto');

// Gateways are rendered forms that communicate directly with the backend-api
module.exports = (app, rootPath, redisClient) => {
    const localRoot = rootPath + '/gateway';
    const svcAuthentication = new AuthenticationService();
    const svcSession = new SessionService(redisClient);


    // Authentication
    app.get(localRoot + '/authenticate', (req, res) => {
        res.render("forms/authenticate", {"name": "Dylan"});
    });

    // Enrollment
    app.get(localRoot + '/enroll', (req, res) => {
        res.render("forms/enroll", {"name": "Dylan"});
    });

    // Authentication
    app.get(localRoot + '/authorize', (req, res) => {
        res.render("forms/authorize", {"name": "Dylan"});
    });

    // Verify - Avoids displaying user input error, redirects on successful run
    app.get(localRoot + '/session', async (req, res, next) => {
        try {
            const sessionKey = req.query.s || null;
            return res.render("forms/sessionFetcher", {"sessionKey": sessionKey});
        } catch (err) { return next(err); }
    });

    // Verify - Avoids displaying user input error, redirects on successful run
    app.get(localRoot + '/verify', async (req, res, next) => {
        try {
            const redirectUri = util.hexToUtf8(req.query.r || "");
            // TODO: Do domain validation
            const credentials = await svcAuthentication.verifyEmail(util.hexToUtf8(req.query.i || ""), req.query.k || null);
            const session = await svcSession.storeSession(credentials._id);
            const iv = crypto.randomBytes(16);
            const cipher = crypto.createCipheriv('aes256-cbc', Buffer.from(session.serverSecret), iv);
            const encrypted = cipher.update(JSON.stringify(session), 'utf8', 'hex') + cipher.final('hex');
            const serverSession = {
                serverSecret: session.serverSecret,
                sessionToken: session.sessionToken,
                sessionKey: session.sessionKey,
                ttl: session.ttl,
                expiry: session.expiry,
                permits: session.permits
            }
            const clientSession = {
                clientSecret: session.clientSecret,
                sessionToken: session.sessionToken,
                sessionKey: session.sessionKey,
                ttl: session.ttl,
                expiry: session.expiry,
                permits: session.permits
            }
            const serverEncoded = Buffer.from(JSON.stringify(serverSession)).toString('base64url')
            const clientEncoded = Buffer.from(JSON.stringify(clientSession)).toString('base64')
            return res.render("forms/sessionFetcher", {
                "sessionKey": clientSession.sessionKey,
                "clientSession": clientEncoded,
                "redirect": `${redirectUri}?s=${serverEncoded}`
            });
        } catch (err) { return next(err); }
    });
}