const crypto = require("crypto");
const CONFIG = require("../../config");


const CreateSession = (
    _credentialsID,
    ttl=CONFIG.session_default_ttl,
    permits=[],
    token=crypto.randomBytes(16).toString('hex'),
    clientSecret=crypto.randomBytes(256).toString('hex'),
    serverSecret=crypto.randomBytes(256).toString('hex'),
    expiry=(ttl || 0) > 0 ? parseInt((+new Date)/1000)+ttl : null,
    key=`fog.sessions:${_credentialsID}:${token}`
) => {
    return {
        _credentialsID: _credentialsID,
        sessionToken: token,
        clientSecret: clientSecret, // Used for the end-user to tell their instance what the remote server is allowed to touch, the remote server should never see this key.
        serverSecret: serverSecret, // Used for the remote server to communicate with the end-user's instance
        expiry: expiry,
        ttl: ttl,
        sessionKey: key,
        permits: permits // Permits are used to grant a session temporary permission to a resource
    }
}

module.exports = CreateSession;