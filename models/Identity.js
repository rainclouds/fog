import mongoose from 'mongoose';


class Identity extends mongoose.Schema {
    constructor() {
        return super({
            contactName: {
                type: String,
                unique: true,
                required: true
            },
            displayName: {
                type: String,
                unique: false,
                required: true
            },
            firstName: {
                type: String,
                unique: false,
                required: false
            },
            lastName: {
                type: String,
                unique: false,
                required: false
            },
            email: {
                type: String,
                unique: false,
                required: false
            },
            phone: {
                type: String,
                unique: false,
                required: false
            },
            _credentials: {
                type: mongoose.Schema.Type.ObjectId,
                ref: 'credentials',
                required: true
            }
        });
    }
}

export default mongoose.model('identity', new Identity)