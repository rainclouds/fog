const mongoose = require('mongoose');
const CONFIG = require("../config")


class Credentials extends mongoose.Schema {
    constructor() {
        return super({
            signOn: {
                type: String,
                unique: true,
                required: true
            },
            hash: {
                type: String,
                unique: false,
                required: true
            },
            email: {
                type: String,
                unique: true,
                required: CONFIG.enrollment_rules.require_email
            },
            emailVerified: {
                type: Boolean,
                required: true,
                default: false
            },
            emailVerificationCode: {
                type: String,
                required: true
            },
            phone: {
                type: String,
                unique: true,
                required: CONFIG.enrollment_rules.require_phone
            },
            phoneVerified: {
                type: Boolean,
                required: true,
                default: false
            }
        });
    }
}

module.exports = mongoose.model('credentials', new Credentials());