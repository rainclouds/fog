/* LOAD ENVIRONMENT VARIABLES */
require('dotenv').config();

/* External packages */
const express = require('express');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const https = require('https')
const http = require('http');
const redis = require('redis');
const conf = require('./config');
const bodyParser = require('body-parser');

const listenHttp = (app, address=conf.service_http_address, port=conf.service_http_port, root=conf.service_root) => {
    http.createServer(app).listen(port, address);
    console.log("Fog authentication https service bound to ", util.format("http://%s:%s%s", address, port, root));
}

const listenHttps = (app, address=conf.service_https_address, port=conf.service_https_port, cert=conf.service_certificate, root=conf.service_root) => {
    https.createServer(cert, app).listen(port, address);
    console.log("Fog authentication https service bound to ", util.format("https://%s:%s%s", address, port, root));
}

const startRedis = async (url=conf.redis_conn_str, url_masked=conf.redis_conn_str_masked) => {
    const redisClient = redis.createClient({ url: url });
    console.log(`Creating connection to Redis, ${url_masked}`)
    redisClient.on('error', err => console.error('Redis Client Error', err));
    await redisClient.connect();
    return redisClient;
}

const startMongoose = async (url=conf.mongo_conn_str, masked_url=conf.mongo_conn_str_masked, options=conf.mongo_options) => {
    await mongoose.connect(url, options);
    console.log(`Creating connection to MongoDB, ${masked_url}`);
}

const bindRoutes = (app, redisClient) => {
    fs.readdirSync(__dirname + '/routes').forEach((file) => {
        module.exports[path.basename(file, '.js')] = require(path.join(__dirname + '/routes', file))(app, conf.service_root, redisClient)
    });
}

const handleValidationError = (err, res, req) => {
    return res.status(400).json({status: 400, message: "Bad Request", validations: err.validationErrors})
}

const handleSystemError = (err, res, req) => {
    const statusCode = err.status || 500;
    const errorMessage = err.message || "An unknown error has occurred";
    console.error(statusCode, err); // TODO: Implement a real custom logging here
    if(statusCode === 500) return res.status(500).json({status: statusCode, message: "An internal server error has occurred! Please contact the network administrator, or try again later!"});
    if (statusCode === 401) return res.set('WWW-authenticate', err.wwwAuthenticate).status(401).json({status: 401, message: err.message});
    return res.status(statusCode).json({status: statusCode, message: errorMessage}); // All other status codes permit custom messages, as they are generated from these services made with the intent that they are for the end-user to view.
}

const httpErrorHandler = (err, req, res, next) => {
    if(err.name === "JsonSchemaValidationError") handleValidationError(err, res, req);
    else handleSystemError(err, res, req);
}

const main = async () => {
    const app = express();
    const redisClient = await startRedis();
    await startMongoose();
    app.set("view engine", "pug");
    app.set("views", path.join(__dirname + '/views'));
    app.use(cookieParser())
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: false}));
    app.use(`${conf.service_root}/public`, express.static('public'));
    app.use(httpErrorHandler);
    bindRoutes(app, redisClient);
    if(conf.serve_http) listenHttp(app);
    if(conf.serve_https) listenHttps(app);
    if(!conf.serve_http || !conf.serve_https) throw Error("Fog must serve on at least 1 protocol (http/https).");
}

main().then((text) => {
    console.log("Fog authentication service has successfully started!");
}).catch((err) => {
    console.error("Tried launching Fog Authentication service but caught errors in the process...");
    console.error(err);
    console.error(JSON.stringify(err, ["message", "arguments", "type", "name"]));
});
