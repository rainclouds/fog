const Credentials = require ("../models/Credentials");


class CredentialsRepository {
    constructor(){}
    async create(document) { return await Credentials.create(document); }
    async findByID(_credentialsID) { return await Credentials.findOne({_id: _credentialsID}); }
    async findByEmail(email) { return await Credentials.findOne({email: email.toLowerCase()}); }
    async findBySignOnAndHash(signOn, hash) { return await Credentials.findOne({signOn: signOn.toLowerCase(), hash: hash}); }
    async signOnExists(signOn) { return await Credentials.countDocuments({signOn: signOn.toLowerCase()}) > 0; }
    async emailExists(email) { return await Credentials.countDocuments({email: email.toLowerCase()}) > 0; }
    async validEmailCode(email, code) { return await Credentials.countDocuments({email: email.toLowerCase(), emailVerificationCode: code, emailVerified: false}) > 0; }
    async updateEmailVerified(email, verified=true) { await Credentials.updateOne({email: email.toLowerCase()}, {emailVerified: verified}); }
}

module.exports = CredentialsRepository;