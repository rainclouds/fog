const nodemailer = require('nodemailer');
const CONFIG = require("../config");
const pug = require("pug");
const util = require('../util')

class EmailService {
    constructor(transporterOptions) {
        this.transporter = nodemailer.createTransport(transporterOptions);
        this.templates = {
            verify: pug.compileFile(CONFIG.email_templates.verification.pug)
        }
    }

    async _sendMail(mailOptions, transporter=this.transporter) {
        return new Promise(function (resolve, reject){
            transporter.sendMail(mailOptions, (err, info) => {
                if (err) return reject(err);
                resolve(info);
                console.log(`Fog sent an email to ${mailOptions.to}; {subject: ${mailOptions.subject}}`);
            });
        });
    }

    async send(to, subject, text=null, html=null, from=CONFIG.system_from_address) {
        return await this._sendMail( {from, to, subject, text, html});
    }

    async sendVerificationMail(to, verificationCode, host) {
        const url = `${host}/gateway/verify?k=${verificationCode}&i=${util.utf8ToHex(to)}`;
        //const text = CONFIG.email_templates.verification.text TODO: Integrate raw text
        const html = this.templates.verify({"verificationUrl": url});
        return await this.send(to, CONFIG.email_templates.verification.subject, null, html);
    }
}

module.exports = EmailService;