const HttpError = require("../HttpError");
const Session = require('../models/redis/Session')


class SessionService {
    constructor(redisClient) {
        this.redisClient = redisClient;
    }

    // Update the token expiration client and server keys.
    async refreshSession(session) {
        await this.popSession(session.sessionKey);
        session = Session(session._credentialsID, session.ttl, session.permits, session.sessionToken);
        return await this.storeSession(session._credentialsID, session);
    }

    // Creating a permit reconstructs the session object
    async createPermit(credentials, sessionToken, permit){
        let session = this.fetchSession(credentials._id, sessionToken);
        session.permits.push(permit);
        return await this.refreshSession(session);
    }

    async storeSession(_credentialsID, session=Session(_credentialsID)) {
        await this.redisClient.set(session.sessionKey, JSON.stringify(session));
        if(session.sessionTTL > 0) await this.redisClient.expire(session.sessionKey, session.expiry);
        console.log(`Session Created fog.sessions:${_credentialsID}:${session.sessionToken}`);
        return this.fetchSession(session.sessionKey, false); // This is technically unnecessary, but it makes for a good assertion that redis is working.
    }

    async fetchSession(sessionKey, refresh=true) {
        if (!refresh) return JSON.parse(await this.redisClient.get(sessionKey));
        else return await this.refreshSession(JSON.parse(await this.redisClient.get(sessionKey)))
    }

    async fetchSessionByIdAndToken(_credentialsID, sessionToken) {
        return this.fetchSession(`fog.sessions:${_credentialsID}:${sessionToken}`);
    }

    async popSession(sessionKey) {
        await this.redisClient.del(sessionKey);
    }

    async popSessionByIdAndToken(_credentialsID, sessionToken) {
        await this.popSession(`fog.sessions:${_credentialsID}:${sessionToken}`);
    }

    // Throws an error if the client session is invalid, does nothing otherwise
    validateClientSession(session, clientSecret) {
        if(session.expiry <= parseInt((+new Date)/1000)) throw new HttpError(440, "Session expired");
        if(session.clientSecret !== clientSecret) throw new HttpError(401, "Unauthorized");
    }

    // Throws an error if the server session is invalid, does nothing otherwise
    validateServerSession(session, serverSecret) {
        if(session.expiry <= parseInt((+new Date)/1000)) throw new HttpError(440, "Session expired");
        if(session.serverSecret !== serverSecret) throw new HttpError(401, "Unauthorized");
    }
}

module.exports = SessionService;