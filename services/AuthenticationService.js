const CredentialsRepository = require("../repositories/CredentialsRepository");
const HttpError = require("../HttpError");
const UnauthorizedError = require("../errors/UnauthorizedError");
const CONFIG = require("../config");
const crypto = require("crypto");


class AuthenticationService {
    constructor() {
        this.credentialsResporitory = new CredentialsRepository();
    }

    async authenticate(signOn, hash) {
        const credentials = await this.credentialsResporitory.findBySignOnAndHash(signOn, hash);
        if(!credentials) throw new UnauthorizedError("Invalid Login", 'Basic realm="fog.login"');
        const emailVerified = !CONFIG.enrollment_rules.require_email_verification || credentials.emailVerified;
        if(!emailVerified) throw new UnauthorizedError("Unverified Email", 'Basic realm="fog.login"');
        return credentials;
    }

    async enroll(signOn, hash, email, phone) {
        const emailVerificationCode = crypto.randomBytes(24).toString('hex')
        const document = {signOn, hash, email: email.toLowerCase(), phone, emailVerificationCode};
        const signOnExists = await this.credentialsResporitory.signOnExists(signOn);
        const emailExists = email && await this.credentialsResporitory.emailExists(email);
        if (signOnExists || emailExists) throw new HttpError(409, "That Email or SignOn already exists");
        else return await this.credentialsResporitory.create(document);
    }

    async verifyEmail(email, key) {
        if(!email) throw new HttpError(401, "Unauthorized");
        if(!await this.credentialsResporitory.validEmailCode(email, key)) throw new HttpError(401, "Unauthorized");
        await this.credentialsResporitory.updateEmailVerified(email, true);
        return await this.credentialsResporitory.findByEmail(email);
    }
}

module.exports = AuthenticationService;