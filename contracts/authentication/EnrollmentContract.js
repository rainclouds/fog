const CONFIG = require("../../config");

const EnrollmentContract = {
    body: {
        type: "object",
        required: [],
        properties: {
            phone: {
                type: "string",
                minLength: 1
            },
            email: {
                type: "string",
                minLength: 1
            }
        }
    }
}

if(CONFIG.enrollment_rules.require_email) EnrollmentContract.body.required.push("email");
if(CONFIG.enrollment_rules.require_phone) EnrollmentContract.body.required.push("phone");

module.exports = EnrollmentContract;